package ro.madalin.Web.Shop.Tabaco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebShopTabacoApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebShopTabacoApplication.class, args);
	}

}
