package ro.madalin.Web.Shop.Tabaco.database;

import org.springframework.stereotype.Repository;

@Repository
public class UserMail extends User {

    private String email;

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }
}
