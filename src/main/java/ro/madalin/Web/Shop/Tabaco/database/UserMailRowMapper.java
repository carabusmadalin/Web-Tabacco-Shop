package ro.madalin.Web.Shop.Tabaco.database;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMailRowMapper implements RowMapper<UserMail> {
    @Override
    public UserMail mapRow(ResultSet resultSet, int i) throws SQLException {
        UserMail userMail = new UserMail();

        userMail.setEmail(resultSet.getString("email"));


        return userMail;
    }
}
