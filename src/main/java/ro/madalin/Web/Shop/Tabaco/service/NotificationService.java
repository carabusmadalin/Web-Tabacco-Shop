package ro.madalin.Web.Shop.Tabaco.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ro.madalin.Web.Shop.Tabaco.database.OrderDao;
import ro.madalin.Web.Shop.Tabaco.database.UserMail;
import ro.madalin.Web.Shop.Tabaco.security.UserSession;


@Service
public class NotificationService {

    private JavaMailSender javaMailSender;

    @Autowired
    UserMail userMail;

    @Autowired
    UserService userService;
    @Autowired
    OrderDao orderDao;
    @Autowired
    UserSession userSession;

    @Autowired
    public NotificationService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendNotification(String email) throws MailException {
        //send email
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(userMail.getEmail());
        mail.setFrom("carabusmadalin@gmail.com");
        mail.setSubject("Inregistrare completa");
        mail.setText("Draga client,\n" +
                "\n" +
                "Iti multumim pentru alegerea site-ul si ne bucuram sa te cunoastem!\n" +
                "Iti confirmam ca ai fost inregistrat cu succes.\n" +
                "\n" +
                "Multumim,\n" +
                "Echipa TabaccoRO");

        javaMailSender.send(mail);
    }

    public void sendAlertPassword(String email) throws MailException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(userMail.getEmail());
        mail.setFrom("carabusmadalin@gmail.com");
        mail.setSubject("Modificare parola");
        mail.setText("Draga client,\n" +
                "\n" +
                "Iti confirmam ca parola a fost schimbata!\n" +
                "Daca nu ai schimbat tu parola te rog sa ne contactezi.\n" +
                "Daca ai schimbat tu parola te rog sa ignori mesajul.\n" +
                "\n" +
                "Multumim,\n" +
                "Echipa TabaccoRO");

        javaMailSender.send(mail);
    }

    public void sendAlertAddress(String email) throws MailException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(userMail.getEmail());
        mail.setFrom("carabusmadalin@gmail.com");
        mail.setSubject("Modificare adresa");
        mail.setText("Draga client,\n" +
                "\n" +
                "Iti confirmam ca adresa de livrare a fost schimbata!\n" +
                "\n" +
                "Multumim,\n" +
                "Echipa TabaccoRO");

        javaMailSender.send(mail);
    }

    public void sendAlertOrder(String email) throws MailException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(userMail.getEmail());
        mail.setFrom("carabusmadalin@gmail.com");
        mail.setSubject("Confirmare plasare comanda");
        mail.setText("Draga client,\n" +
                "\n" +
                "Iti confirmam pasarea comenzii nr." + userService.orderUser()+ ".\n" +
                "Va vom contacta in cel mai scurt timp posibil.\n" +
                "\n" +
                "Multumim,\n" +
                "Echipa TabaccoRO\n");

        javaMailSender.send(mail);

    }
}
