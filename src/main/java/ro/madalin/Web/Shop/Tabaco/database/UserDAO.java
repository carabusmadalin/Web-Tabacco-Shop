package ro.madalin.Web.Shop.Tabaco.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ro.madalin.Web.Shop.Tabaco.security.UserSession;

import java.util.List;

@Repository
public class UserDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    UserSession userSession;
    @Autowired
    UserMail userMail;


    public List<User> findByEmail(String email) {
        return jdbcTemplate.query("select * from users where email='" + email + "';", new UserRowMapper());
    }

    public void save(String email, String password, String address) {
        jdbcTemplate.update("insert into users values(null, ?, ?, ?)", email, password, address);
    }

    public void savePassword(String password) {
        jdbcTemplate.update("update users set password ='" + password + "' where id=" + userSession.getUserId() + "");
    }

    public void saveNewAddress(String newAddress) {
        jdbcTemplate.update("update users set address ='" + newAddress + "' where id=" + userSession.getUserId() + "");
    }

    public List<UserMail> findByUserEmail() {
       return jdbcTemplate.query("select * from users where id =" + userSession.getUserId() , new UserMailRowMapper());

    }

}