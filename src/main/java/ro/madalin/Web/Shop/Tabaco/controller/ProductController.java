package ro.madalin.Web.Shop.Tabaco.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ro.madalin.Web.Shop.Tabaco.database.*;
import ro.madalin.Web.Shop.Tabaco.security.UserSession;
import ro.madalin.Web.Shop.Tabaco.service.NotificationService;
import ro.madalin.Web.Shop.Tabaco.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class ProductController {

    @Autowired
    ProductDAO productDAO;

    @Autowired
    UserMail userMail;

    @Autowired
    UserSession userSession;

    @Autowired
    NotificationService notificationService;

    @Autowired
    CategoryDAO categoryDAO;

    @Autowired
    UserService userService;

    @Autowired
    CartProduct cartProduct;


    @GetMapping("/product")
    public ModelAndView product(@RequestParam("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView("product");

        Product product = productDAO.findById(id);
        modelAndView.addObject("product", product);
        int productCounter = 0;
        for (int quantityForProduct : userSession.getShoppingCart().values()) {
            productCounter = productCounter + quantityForProduct;
        }
        modelAndView.addObject("shoppingCartSize", productCounter);
        cartProduct.setSizeCard(productCounter);
        List<Category> categories = categoryDAO.findAllCategory();
        modelAndView.addObject("categories", categories);
        modelAndView.addObject("shoppingCartSize", cartProduct.getSizeCard());

        return modelAndView;
    }

    @PostMapping("/add-to-cart")
    public ModelAndView addToCart(@RequestParam("productId") Integer id) {
        if (userSession.getUserId() == 0) {
            return new ModelAndView("redirect:/log-in");
        } else
            userSession.addNewProduct(id);

        return new ModelAndView("redirect:product?id=" + id);
    }

    @GetMapping("shopping-cart")
    public ModelAndView shoppingCart() {

        ModelAndView modelAndView = new ModelAndView("cart");
        List<CartProduct> productsFromCart = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : userSession.getShoppingCart().entrySet()) {
            int quantity = entry.getValue();
            int productId = entry.getKey();
            Product productFromDatabase = productDAO.findById(productId);
            CartProduct cartProduct = new CartProduct();
            cartProduct.setQuantity(quantity);
            cartProduct.setId(productFromDatabase.getId());
            cartProduct.setName(productFromDatabase.getName());
            cartProduct.setPhotoFile(productFromDatabase.getPhotoFile());
            cartProduct.setPrice(productFromDatabase.getPrice());

            productsFromCart.add(cartProduct);
        }
        List<Category> categories = categoryDAO.findAllCategory();
        modelAndView.addObject("categories", categories);
        modelAndView.addObject("products", productsFromCart);
        modelAndView.addObject("shoppingCartSize", cartProduct.getSizeCard());

        return modelAndView;
    }

    @GetMapping("save-cart")
    public ModelAndView saveCart() {
        userService.saveDataBase();
        notificationService.sendAlertOrder(userMail.getEmail());
        userService.clearCart();
        return new ModelAndView("redirect:dashboard");
    }

}
