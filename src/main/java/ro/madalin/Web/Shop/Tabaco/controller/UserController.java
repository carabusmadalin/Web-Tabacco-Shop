package ro.madalin.Web.Shop.Tabaco.controller;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ro.madalin.Web.Shop.Tabaco.database.*;
import ro.madalin.Web.Shop.Tabaco.security.UserSession;
import ro.madalin.Web.Shop.Tabaco.service.NotificationService;
import ro.madalin.Web.Shop.Tabaco.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserDAO userDAO;

    @Autowired
    ProductDAO productDAO;

    @Autowired
    CategoryDAO categoryDAO;

    @Autowired
    UserMail userMail;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    User user;

    @Autowired
    UserSession userSession;

    @Autowired
    OrderDao orderDao;

    @Autowired
    CartProduct cartProduct;

    @GetMapping("/register-form")
    public ModelAndView registerAction(@RequestParam("email") String email,
                                       @RequestParam("password") String password,
                                       @RequestParam("password-again") String password2,
                                       @RequestParam("address") String address) {
        ModelAndView modelAndView = new ModelAndView("register");
        if (!password.equals(password2)) {
            modelAndView.addObject("message", "Parole nu sunt identice!");
            return modelAndView;
        } else {
            try {
                userService.save(email, password, address);
            } catch (InvalidPassword invalidPassword) {
                String messageException = invalidPassword.getMessage();
                modelAndView.addObject("message", messageException);
                return modelAndView;
            }
        }

        user.setEmail(email);
        notificationService.sendNotification(email);

        return new ModelAndView("redirect:/index.html");
    }

    @GetMapping("/register")
    public ModelAndView register() {
        return new ModelAndView("register");
    }

    @PostMapping("/login")
    public ModelAndView login(@RequestParam("email") String email,
                              @RequestParam("password") String password,
                              HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("index");
        List<User> userList = userService.findByEmail(email);
        if (userList.size() == 0) {
            modelAndView.addObject("message", "Credentialele nu sunt corecte!");
        }
        if (userList.size() > 1) {
            modelAndView.addObject("message", "Credentialele nu sunt corecte!");
        }
        if (userList.size() == 1) {
            User userFromDatabase = userList.get(0);
            if (!userFromDatabase.getPassword().equals(DigestUtils.md5Hex(password))) {
                modelAndView.addObject("message", "Credentialele nu sunt corecte!");
            } else {
                userSession.setUserId(userFromDatabase.getId());
                modelAndView = new ModelAndView("redirect:/dashboard");
            }
            userMail.setEmail(email);
        }
        return modelAndView;
    }

    @GetMapping("dashboard")
    public ModelAndView dashboard() {
        if (userSession.getUserId() == 0) {
            return new ModelAndView("redirect:/index.html");
        }

        List<Product> products = productDAO.findAll();
        for (Product p : products) {
            p.setUrl("product?id=" + p.getId());
        }
        List<Category> categories = categoryDAO.findAllCategory();
        ModelAndView modelAndView = new ModelAndView("dashboard");
        modelAndView.addObject("products", products);
        modelAndView.addObject("categories", categories);
        modelAndView.addObject("shoppingCartSize", cartProduct.getSizeCard());


        return modelAndView;
    }

    @GetMapping("/my-orders-history")
    public ModelAndView history() {
        List<Order> orders = orderDao.findOrderForUser(userSession.getUserId());
        ModelAndView modelAndView = new ModelAndView("orders");
        modelAndView.addObject("orders", orders);
        List<Category> categories = categoryDAO.findAllCategory();
        modelAndView.addObject("categories", categories);
        modelAndView.addObject("shoppingCartSize", cartProduct.getSizeCard());
        return modelAndView;
    }

    @GetMapping(value = "/download-order-history", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public byte[] download(HttpServletResponse httpServletResponse) throws Exception {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();

        Row firstRow = sheet.createRow(0);
        firstRow.createCell(0).setCellValue("Id comanda");
        firstRow.createCell(1).setCellValue("Pret total");

        FileOutputStream fileOutputStream = new FileOutputStream("raport");
        workbook.write(fileOutputStream);
        fileOutputStream.close();
        workbook.close();

        httpServletResponse.setHeader("Content-Disposition", "attachment; filename=\"report.xls\"");

        return new FileInputStream("raport").readAllBytes();
    }

    @GetMapping("/log-in")
    public ModelAndView up() {
        return new ModelAndView("index");
    }

    @GetMapping("category")
    public ModelAndView category(@RequestParam("idC") Integer idC,
                                 HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("category");
        List<Product> productList = productDAO.findByCategoryId(idC);
        for (Product p : productList) {
            p.setUrl("product?id=" + p.getId());
        }
        List<Category> categories = categoryDAO.findAllCategory();
        modelAndView.addObject("categories", categories);
        modelAndView.addObject("productList", productList);

        modelAndView.addObject("shoppingCartSize", cartProduct.getSizeCard());


        return modelAndView;
    }

    @GetMapping("/change")
    public ModelAndView changeUser() {
        ModelAndView modelAndView = new ModelAndView("change");
        modelAndView.addObject("shoppingCartSize", cartProduct.getSizeCard());
        return modelAndView;
    }

    @PostMapping("/password")
    public ModelAndView registerAction(@RequestParam("Password") String password,
                                       @RequestParam("Password-again") String password2) {
        ModelAndView modelAndView = new ModelAndView("change");
        if (!password.equals(password2)) {
            modelAndView.addObject("message", "Parole nu sunt identice!");
            return modelAndView;
        } else {
            try {
                userService.updatePassword(password);
            } catch (InvalidPassword invalidPassword) {
                String messageException = invalidPassword.getMessage();
                modelAndView.addObject("message", messageException);
                return modelAndView;
            }

            notificationService.sendAlertPassword(userMail.getEmail());
        }
        return new ModelAndView("redirect:/change");
    }

    @PostMapping("/address")
    public ModelAndView registerAction(@RequestParam("newAddress") String newAddress) {
        ModelAndView modelAndView = new ModelAndView("change");
        userDAO.saveNewAddress(newAddress);
        notificationService.sendAlertAddress(userMail.getEmail());
        return new ModelAndView("redirect:/change");
    }
    @GetMapping("/log-out")
    public ModelAndView logOut(){
        userSession.setUserId(0);
            return new ModelAndView("redirect:/index.html");

    }
}

