package ro.madalin.Web.Shop.Tabaco.database;

import org.springframework.stereotype.Repository;


@Repository
public class CartProduct extends Product {

    private int quantity;
    private Integer sizeCard;
    private String clearCard;

    public String getClearCard() {
        return clearCard;
    }

    public void setClearCard(String clearCard) {
        this.clearCard = clearCard;
    }

    public Integer getSizeCard() {
        return sizeCard;
    }

    public void setSizeCard(Integer sizeCard) {
        this.sizeCard = sizeCard;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void cos(){}
}
