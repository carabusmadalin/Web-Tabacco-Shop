package ro.madalin.Web.Shop.Tabaco.database;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Category> findAllCategory() {
        return jdbcTemplate.query("select * from category", new CategoryRowMapper());
    }

}
