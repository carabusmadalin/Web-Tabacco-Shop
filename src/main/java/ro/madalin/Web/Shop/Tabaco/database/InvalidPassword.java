package ro.madalin.Web.Shop.Tabaco.database;

public class InvalidPassword extends Exception {

    public InvalidPassword(String message) {
        super(message);
    }
}
