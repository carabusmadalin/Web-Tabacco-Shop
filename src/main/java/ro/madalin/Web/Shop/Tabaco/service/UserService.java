package ro.madalin.Web.Shop.Tabaco.service;


import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.madalin.Web.Shop.Tabaco.database.*;
import ro.madalin.Web.Shop.Tabaco.security.UserSession;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserDAO userDAO;

    @Autowired
    UserSession userSession;

    @Autowired
    OrderDao orderDao;

    @Autowired
    CartProduct cartProduct;

    public void save(String email, String password, String address) throws InvalidPassword {
        if (password.length() < 5) {
            throw new InvalidPassword("parola trebuie sa aibe peste 5 caractere");
        }

        if (userDAO.findByEmail(email).size() > 0) {
            throw new InvalidPassword("exista deja un utilizator cu acest email");
        }
        String passwordMD5 = DigestUtils.md5Hex(password);
        userDAO.save(email, passwordMD5, address);
    }

    public List<User> findByEmail(String email) {
        return userDAO.findByEmail(email);
    }

    public void clearCart() {
        userSession.getShoppingCart().clear();
        cartProduct.setSizeCard(0);

    }

    public void saveDataBase() {
        orderDao.newOrder(userSession.getUserId(), userSession.getShoppingCart());
    }

    public void updatePassword(String password) throws InvalidPassword {
        if (password.length() < 5) {
            throw new InvalidPassword("parola trebuie sa aibe peste 5 caractere");
        }

        String passwordMD5 = DigestUtils.md5Hex(password);
        userDAO.savePassword(passwordMD5);
    }

    public String toString() {
        List<Order> orders = orderDao.findOrderForUser(userSession.getUserId());
        return orders.get(0) + "";
    }

    public int orderUser() {
        int orderId = orderDao.findOrdersLastId();
        return orderId;
    }

}

